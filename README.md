# 数据结构课程资源
## 代码资源
### 开发环境
	CLion + Gcc8
### 代码结构
	按章节名称命名目录，CMakeList.txt文件记录是make工具用来编译代码的文件。
### 代码列表	
* 第一章
	* Ch1/Ch1-1.cpp：计算机程序的基本逻辑结构
	* Ch1/Ch1-2.cpp：抽象数据类型——矩阵
	* Ch1/Ch1-3.cpp：抽象数据类型——矩阵——面向对象实现
	* Ch1/Ch1-4.cpp：算法实例——求n个元素中的最大值
	* Ch1/Ch1-5.cpp：时间复杂度——累加求和
	* Ch1/Ch1-6.cpp：时间复杂度——矩阵相加
	* Ch1/Ch1-7.cpp：时间复杂度——简单选择排序

* 第二章
	* Ch2/Ch2-1.cpp：线性表的顺序存储——面向过程实现
	* Ch2/Ch2-2.cpp：线性表的顺序存储——面向对象实现
	* Ch2/Ch2-3.cpp：线性表的链接存储——面向过程实现
	* Ch2/Ch2-4.cpp：线性表的链接存储——面向过程实现-带头指针
* 第三章
	* Ch3/Ch3-1.cpp：栈的顺序存储
	* Ch3/Ch3-2.cpp：栈的链接存储
	* Ch3/Ch3-3.cpp：栈的应用——键盘输出逆序输出
	* Ch3/Ch3-4.cpp：栈的应用——语法检查
	* Ch3/Ch3-5.cpp：栈的应用——运算表达式求值
	* Ch3/Ch3-6.cpp：栈与递归——递归方法计算阶乘
	* Ch3/Ch3-7.cpp：栈与递归——汉诺塔
* 第四章
    * Ch4/Ch4-1.cpp：二叉树-链接存储
    * Ch4/Ch4-2.cpp：二叉搜索树
    * Ch4/Ch4-3.cpp：堆
    * Ch4/Ch4-4.cpp：哈夫曼树
* 第五章
    * 图的存储——邻接矩阵：   Ch5/Ch5-1.h Ch5/Ch5-1.cpp)
    * 图的存储——邻接表：     Ch5/Ch5-2.cpp Ch5/Ch5-2.h)
    * 图的存储——边集数组：   Ch5/Ch5-3.cpp Ch5/Ch5-3.h)
    * 图的遍历：            Ch5/Ch5-4.cpp Ch5/Ch5-1.h Ch5/Ch5-2.h Ch5/Ch5-4.h)
    * 图的最小生成树：       Ch5/Ch5-5.cpp Ch5/Ch5-1.h Ch5/Ch5-3.h)
    * 最短路径：            Ch5/Ch5-6.cpp Ch5/Ch5-1.h Ch5/Ch5-3.h)
## 课件
lecture目录下
数据结构.png：整体思维导图

## 视频资源
* [数据结构06-线性表的链接存储](https://www.bilibili.com/video/BV1CY4y1h7gd/)
* [数据结构07-线性表的链接存储2](https://www.bilibili.com/video/BV1A5411m7jX/)
* [数据结构08-链表的插入和删除、栈](https://www.bilibili.com/video/BV1K34y1i7iB/)
* [数据结构09-栈的顺序和链接存储](https://www.bilibili.com/video/BV1NT4y1Y7Rw/)
* [数据结构09-栈的顺序和链接存储实现](https://www.bilibili.com/video/BV1Q44y1P7e2/)
* [数据结构10-作业4-2及栈的应用-键盘输入逆置](https://www.bilibili.com/video/BV1gZ4y1171A/)
* [数据结构11-栈的应用：语法检查、算术表达式计算](https://www.bilibili.com/video/BV1ZR4y1N7T8/)
* [数据结构12-栈的应用：算术表达式计算（中缀转后缀）](https://www.bilibili.com/video/BV1FT4y1a7xz/)
* [数据结构13-栈的应用：算术表达式计算（后缀表达式求值）|栈和递归](https://www.bilibili.com/video/BV1dZ4y1y7Wb/)
* [数据结构14-递归 | 队列](https://www.bilibili.com/video/BV1gS4y1a7zx/)
* [数据结构15-树、二叉树的基本概念及存储](https://www.bilibili.com/video/BV1D44y1g7iY/)
* [数据结构16-二叉树的遍历](https://www.bilibili.com/video/BV1cS4y1w7p3/)
* [数据结构16-二叉树的分层遍历、通过广义表建立二叉树](https://www.bilibili.com/video/BV15R4y1P71C/)
* [数据结构17-二叉树的常用算法、还原二叉树、树转二叉树](https://www.bilibili.com/video/BV1xa411a7Vg/)
* [数据结构17-二叉搜索树](https://www.bilibili.com/video/BV1J54y1f7ff/)
* [数据结构18-二叉搜索树2](https://www.bilibili.com/video/BV1hU4y1U7Zq/)
* [数据结构18-堆](https://www.bilibili.com/video/BV1ru411r7bD/)
* [数据结构19-哈夫曼树和哈夫曼编码](https://www.bilibili.com/video/BV14A4y1f76Y/)
* [数据结构20-哈夫曼编码](https://www.bilibili.com/video/BV1DR4y1c74t/)
* [数据结构20-图的基本概念](https://www.bilibili.com/video/BV1ZT4y1B715/)
* [数据结构21-习题6-1](https://www.bilibili.com/video/BV1W34y1E7HR/)
* [数据结构21-图的存储：邻接矩阵、邻接表](https://www.bilibili.com/video/BV163411A7RG/)
* [数据结构22-图的存储：边集数组&图的遍历：深度优先、广度优先](https://www.bilibili.com/video/BV1yU4y127AR/)
* [数据结构22-图的应用：最小生成树（Prim算法）](https://www.bilibili.com/video/BV15S4y1q7PZ/)
* [数据结构23-图的应用：最小生成树（Kruskal算法）](https://www.bilibili.com/video/BV1BY4y1B7t6/)
* [数据结构23-图的应用：最短路径（Dijkstra算法）](https://www.bilibili.com/video/BV1k34y1j71H/)

## 作业
作业1：习题1-4

## 实验
### 实验要求
exp/docu目录下
### 实验报告要求
* 提交内容包括：可运行代码(.cpp)+报告(.doc)+运行结果截图
* 文件命名规则：数学17-3-1701010101-张三-实验1.cpp、数学17-3-1701010101-张三-实验1.doc

## 综合训练项目
### 项目要求
training/docu目录下